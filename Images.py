import cv2
from random import randint, choice, shuffle


def all_img(disgusting, funny, neutral, sad, category, categories=2):  # faces or another category
    """
    Функция составляет лист с картинками для дальнейшей работы с ними
    в зависимости от парматеров в листе может быть разный набор картинок
    далее лист перемешивается и из него вытаскиваются картинки для показа в тесте
    :param disgusting: класс картинок, в паре с нейтрал
    :param funny: класс картинок, в паре с грустными
    :param neutral:
    :param sad:
    :param category: какая из категорий выбрана
    :param categories: категорий может быть 2 или 4
    :return: лист с набором картинок
    """
    imgs = []
    if category == 'surprise':
        imgs.extend(disgusting)
        imgs.extend(neutral)
    elif category == 'faces':
        imgs.extend(funny)
        imgs.extend(sad)
    elif categories != 2:
        imgs.extend(funny)
        imgs.extend(sad)
        imgs.extend(disgusting)
        imgs.extend(neutral)
    return list(set(imgs))


class Images:
    """
    Class Images makes work for displaying images on a screen
    and prepare them for processing in QTE and other files
    """
    def __init__(self, time_to_show_image, number_of_images_shown, imgs, random_time=False):
        self.time_to_show_image = time_to_show_image
        self.number_of_images_showm = number_of_images_shown
        self.random_time = random_time
        self.imgs = imgs  # all images with relative paths
        self.keys = []  # list of keys pressed for every image
        self.appeared_images = []  # images used in current session

    def img_show(self, img):
        """
        this method shows full screen images
        :param img: every single image from img_sequence()
        :return: full screen image and pressed key code
        """
        # set full screen
        cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty("window", cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
        cv2.imshow('window', img)

        # make random time for every image to appear
        if self.random_time: self.time_to_show_image = randint(1000, 3000)

        key = cv2.waitKey(self.time_to_show_image)  # time for one image to appear on the screen, then another image
        # waitkey waits for time_to_show_image for keyboard value, returns specific key user presses
        return key

    def img_sequence(self):
        """
        takes list imgs from concatenate_images(), shuffles it
        walks along it and calls main method in the class img_show()
        for showing every image
        also method takes pressed key of every image and append it in list
        :return: list of pressed key in order of shown images
        """
        shuffle(self.imgs)
        for i in range(self.number_of_images_showm):
            current_image = choice(self.imgs)  # рандомно выбираем картинку из листа
            key = self.img_show(cv2.imread('images\\' + current_image))  # регистрируем какую клавишу нажали
            # на текущую картинку
            self.keys.append(key)  # добавляем в лист нажатых клавиш, именно по нему мы будем считать точность юзера
            self.appeared_images.append(current_image)  # не забываем про показанные картинки
        return self.keys, self.appeared_images
