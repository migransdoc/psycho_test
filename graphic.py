import cv2
import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QMessageBox, QDesktopWidget, QMainWindow, QAction,
                             qApp, QHBoxLayout, QVBoxLayout, QGridLayout, QToolTip, QLabel, QLineEdit, QTextEdit,
                             QCheckBox, QComboBox)
from PyQt5.QtGui import QIcon, QFont, QPainter, QColor, QPixmap
from PyQt5.QtCore import QCoreApplication, Qt, pyqtSlot, QRect, QRectF

WIDTH = 300
HEIGHT = 300  # размеры окна приложения


def instructions():  # эта функция стартует в самом начале и показывает картинку с описанием задачи
    cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)  # делаем ее на фулскрин
    cv2.setWindowProperty("window", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    cv2.imshow('window', cv2.imread('instruction.png'))
    key = cv2.waitKey(-1)  # ждем нажатия любой клавиши


def start_test(TIME_TO_SHOW_IMAGE, NUMBER_OF_IMAGES_SHOWN, RANDOM_TIME, NUMBER_OF_IMAGES_MEMORIZED,
                   NUMBER_OF_CATEGORIES, CATEGORY):  # переходит от задания настроек к показу картинок
    import main_file
    if CATEGORY == 'категории': CATEGORY = 'faces'
    if NUMBER_OF_IMAGES_MEMORIZED == 'количество запомненных': NUMBER_OF_IMAGES_MEMORIZED = 2
    if NUMBER_OF_CATEGORIES == 'количество категорий': NUMBER_OF_CATEGORIES = 2
    if TIME_TO_SHOW_IMAGE == 'Время для картинки': TIME_TO_SHOW_IMAGE = 1000
    if NUMBER_OF_IMAGES_SHOWN == '': NUMBER_OF_IMAGES_SHOWN = 20  # работа с возможными ошибками
    # что юзер не ввел верные значения в боксы

    accuracy = main_file.main(int(str(TIME_TO_SHOW_IMAGE)[0])*1000, int(NUMBER_OF_IMAGES_SHOWN), RANDOM_TIME, int(NUMBER_OF_IMAGES_MEMORIZED),
                   int(NUMBER_OF_CATEGORIES), CATEGORY)  # преобразование от формата для юзера в нужный формат
    return accuracy


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.TIME_TO_SHOW_IMAGE = 1000
        self.RANDOM_TIME = False  # make random time for every image to appear
        self.NUMBER_OF_CATEGORIES = 2  # 2 or 4
        self.NUMBER_OF_IMAGES_SHOWN = 4
        self.NUMBER_OF_IMAGES_MEMORIZED = 2
        self.CATEGORY = 'faces'  # const values for test

    def initUI(self):
        QToolTip.setFont(QFont('SansSerif', 10))  # шрифт подсказок на кнопках

        label = QLabel(self)
        pixmap = QPixmap('untitled71.png')
        label.setPixmap(pixmap)  # картинка на весь фон

        showing_time = QComboBox(self)
        showing_time.addItems(['Время для картинки', '1 sec', '2 sec', '3 sec'])  # выпадающая меню
        showing_time.move(WIDTH - 296, HEIGHT - 25)
        showing_time.setToolTip('время показа одной картинки')  # подсказка при наведении мыши на бокс
        showing_time.activated[str].connect(self.onActivatedST)  # обращается к функции, которая сохраняет
        # значение в конст перменную, с которой будет запускаться тест

        random = QCheckBox('random time', self)
        random.move(WIDTH - 296, HEIGHT - 125)
        random.setToolTip('время можно выставить рандомно')  # подсказка при навежении мыши на бокс
        random.stateChanged.connect(self.changeTitle)  # заходит в функцию, которая сохраняет значение в конст
        # переменную, с которой будет запускаться тест

        category = QComboBox(self)
        category.addItems(['категории', 'faces', 'surprise'])  # категории выпадающего меню
        category.move(WIDTH - 296, HEIGHT - 230)
        category.setToolTip('Можно выбрать из картинки 2 категорий')  # всплывающая подсказка
        category.activated[str].connect(self.onActivatedCategory)  # функция сохраняет значение в переменную
        # для дальнйшего запуска теста с этим значением

        categories = QComboBox(self)
        categories.addItems(['количество категорий', '2', '4'])  # категории всплывающего меню
        categories.move(WIDTH - 296, HEIGHT - 75)
        categories.setToolTip('Можно усложнить себе задачу и выбрать несколько категорий')  # всплывающая подсказка
        categories.activated[str].connect(self.onActivatedCategories)  # вызов функции, которая при взаимодействии
        # юзера с боксом сохраняет в переменную значение из бокса и потом с этой переменной запускается тест

        shown_images = QLineEdit(self)  # поле для ввода текста (просто строка)
        shown_images.move(WIDTH - 296, HEIGHT - 275)
        shown_images.setToolTip('Количество показываемых картинок')  # всплывающая подсказка
        shown_images.textChanged[str].connect(self.onChanged)

        memorized_images = QComboBox(self)
        memorized_images.addItems(['количество запомненных', '2', '3', '4', '5'])
        memorized_images.move(WIDTH - 296, HEIGHT - 175)
        memorized_images.setToolTip('Сколько картинок вы хотите запоминать')
        memorized_images.activated[str].connect(self.onActivatedMemory)

        start = QPushButton('start', self)
        start.clicked.connect(self.start_button)  # при нажатии на кнопку запускается функция start_button
        start.resize(start.sizeHint())
        start.move(WIDTH - 75, HEIGHT - 75)  # start button

        exit = QPushButton('exit', self)
        exit.clicked.connect(QCoreApplication.instance().quit)  # при нажатии на кнопку программа завершается
        exit.resize(exit.sizeHint())  # автоматически настраиваемый размер
        exit.move(WIDTH - 75, HEIGHT - 25)  # exit button

        self.resize(WIDTH, HEIGHT)  # ресайзит все окно программы
        self.center()  # по центру экрана
        self.setWindowTitle('psycho test')
        self.show()

    '''def results(self):
        accuracy = start_test(self.TIME_TO_SHOW_IMAGE, self.NUMBER_OF_IMAGES_SHOWN, self.RANDOM_TIME,
                   self.NUMBER_OF_IMAGES_MEMORIZED, self.NUMBER_OF_CATEGORIES, self.CATEGORY)
        self.text = 'Ваш результат' + accuracy
        self.resize(WIDTH, HEIGHT)
        self.center()
        self.setWindowTitle('test results')
        self.show()'''

    '''def closeEvent(self, event):  # if user wanta to close program
        # message box appears is he really wants to quit
        reply = QMessageBox.question(self, 'Exit', 'Вы уверены, что хотите выйти', QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()'''  # при нажатии на крестик всплывает диалоговое окно Вы уверены что хотите выйти

    def center(self):  # window in the center of the screen
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def keyPressEvent(self, e):  # if esc was pressed program closed
        if e.key() == Qt.Key_Escape:
            self.close()

    def changeTitle(self, state):  # цепляется к чекбоксу рандома
        if state == Qt.Checked:
            self.RANDOM_TIME = True
        else:
            self.RANDOM_TIME = False

    def onChanged(self, text):  # цепляется к shown_images, полю для ввода
        self.NUMBER_OF_IMAGES_SHOWN = text

    def onActivatedST(self, text):  # к боксу выбора времени
        self.TIME_TO_SHOW_IMAGE = text

    def onActivatedCategory(self, text):
        self.CATEGORY = text

    def onActivatedCategories(self, text):
        self.NUMBER_OF_CATEGORIES = text

    def onActivatedMemory(self, text):
        self.NUMBER_OF_IMAGES_MEMORIZED = text

    '''def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        self.drawText(event, qp)
        qp.end()

    def drawText(self, event, qp):
        qp.setPen(QColor(168, 34, 3))
        qp.setFont(QFont('Decorative', 10))
        qp.drawText(event.rect(), Qt.AlignCenter, self.text)'''

    def start_button(self):
        self.close()  # закрывает окно приложения, чтоб оно не висело поверх картинок из теста
        start_test(self.TIME_TO_SHOW_IMAGE, self.NUMBER_OF_IMAGES_SHOWN, self.RANDOM_TIME,
                   self.NUMBER_OF_IMAGES_MEMORIZED, self.NUMBER_OF_CATEGORIES, self.CATEGORY)


instructions()  # прежде всего идет показ инструкций
app = QApplication(sys.argv)  # старт процесса с приложением
ex = Example()
sys.exit(app.exec_())  # завершение всей программы
