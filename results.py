# запись результатов в файд вместе с временем их получения
import datetime


def write_res(accuracy):
    with open('results.txt', 'a') as file:
        now = datetime.datetime.now()
        file.write(str(now)[:-10] + ' ' + str(int(accuracy)) + '%' + '\n')
