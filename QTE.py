YES = 113
NO = 119


class QTE:  # класс отвечает за работу с листами картинок и нажатых клавиш
    # здесь происходит подсчет точности
    def __init__(self, appeared_images, pressed_keys, memory):
        self.images = appeared_images  # what images have been shown
        self.keys = pressed_keys  # what keys were pressed
        self.count = 0  # number of right answers
        self.memory = memory  # number of images memorized

    def count_answers(self):
        """ 
        counts correct user answers
        :param
        :return:
        """
        t = -1  # это индекс в листе, идем с конца
        for key in reversed(self.keys):
            try:
                if (key == YES and self.images[t][0] == self.images[t - self.memory][0]) or (
                            key == NO and self.images[t][0] != self.images[t - self.memory][0]):
                    self.count += 1
            except IndexError:  # если юзер случайно нажал больше раз чем нужно, на корректную работу алгоритма
                # это не влияет
                pass

    def accuracy(self):  # user accuracy
        print(self.count, len(self.images), self.memory)
        return (self.count / (len(self.images) - self.memory)) * 100
