Программа проверки памяти и концентрации пользователя
Программа на основе входных данных составляет тест с картинками и считает аккуратность пользователя. Пользователю выводится результат

Программа на основе входных данных составляет сет изображений и показывает их пользователю, требуя нажимать определенные клавиши. После получения характеристик пользователь может вводить соответствующие, корректные данные, исходя из которых подбирается сет. Программа может составлять сет, показывать его, регистрировать нажатые клавиши и считать объем верных ответов