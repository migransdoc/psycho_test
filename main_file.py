from QTE import QTE
from Images import Images, all_img
import cv2


disgusting = ['disgusting\\7380.jpg', 'disgusting\\9290.jpg', 'disgusting\\9291.jpg', 'disgusting\\9295.jpg', 'disgusting\\9300.jpg', 'disgusting\\9301.jpg', 'disgusting\\9302.jpg', 'disgusting\\9320.jpg', 'disgusting\\9322.jpg', 'disgusting\\9332.jpg', 'disgusting\\9341.jpg', 'disgusting\\9342.jpg', 'disgusting\\9373.jpg', 'disgusting\\9390.jpg', 'disgusting\\9402.jpg', 'disgusting\\9611.jpg', 'disgusting\\9832.jpg', 'disgusting\\a.jpg', 'disgusting\\aa.jpg', 'disgusting\\b.jpg', 'disgusting\\c.jpg', 'disgusting\\d.jpg', 'disgusting\\i.jpg', 'disgusting\\j.jpg', 'disgusting\\k.jpg', 'disgusting\\l.jpg', 'disgusting\\m.jpg', 'disgusting\\n.jpg', 'disgusting\\o.jpg', 'disgusting\\p.jpg', 'disgusting\\pp.jpg', 'disgusting\\q.jpg', 'disgusting\\r.jpg', 'disgusting\\s.jpg', 'disgusting\\t.jpg', 'disgusting\\u.jpg', 'disgusting\\v.jpg', 'disgusting\\w.jpg', 'disgusting\\x.jpg', 'disgusting\\y.jpg', 'disgusting\\z.jpg']
funny = ['funny\\e1.png', 'funny\\e2.png', 'funny\\e3.png', 'funny\\e4.png', 'funny\\e5.png', 'funny\\e6.png', 'funny\\h1.png', 'funny\\h2.png', 'funny\\h3.png', 'funny\\h4.png', 'funny\\h5.png', 'funny\\h6.png', 'funny\\h7.png', 'funny\\h8.png', 'funny\\h9.png', 'funny\\IF7_00.05.png', 'funny\\KF7_00.09.png']
neutral = ['neutral\\n1.jpg', 'neutral\\n10.jpg', 'neutral\\n11.jpg', 'neutral\\n12.jpg', 'neutral\\n2.jpg', 'neutral\\n3.jpg', 'neutral\\n4.jpg', 'neutral\\n5.jpg', 'neutral\\n6.jpg', 'neutral\\n7.jpg', 'neutral\\n8.jpg', 'neutral\\n9.jpg']
sad = ['sad\\a1.png', 'sad\\a2.png', 'sad\\a3.png', 'sad\\a4.png', 'sad\\a5.png', 'sad\\a6.png', 'sad\\a7.png', 'sad\\a8.png', 'sad\\d1.png', 'sad\\d2.png', 'sad\\d3.png', 'sad\\d4.png', 'sad\\d5.png', 'sad\\d6.png', 'sad\\d7.png', 'sad\\d8.png']


def main(TIME_TO_SHOW_IMAGE, NUMBER_OF_IMAGES_SHOWN, RANDOM_TIME, NUMBER_OF_IMAGES_MEMORIZED,
         NUMBER_OF_CATEGORIES, CATEGORY):
    imgs = all_img(disgusting, funny, neutral, sad, CATEGORY, NUMBER_OF_CATEGORIES)  # создание листа с требуемыми изображениями из директорий
    images = Images(TIME_TO_SHOW_IMAGE, NUMBER_OF_IMAGES_SHOWN, imgs, RANDOM_TIME)
    keys, appeared_images = images.img_sequence()  # получение нажатых клавиш на показанные картинки
    qte = QTE(appeared_images, keys, NUMBER_OF_IMAGES_MEMORIZED)
    qte.count_answers()  # считаем ответы и точность
    accuracy = qte.accuracy()
    print(accuracy)
    import results
    results.write_res(accuracy)  # запись результатов в файл

    with open('results.txt', 'r') as file:
        accuracies = file.readlines()
        pred_res = accuracies[-2][:-1]  # получаем прощлый результат
        img = cv2.imread('background.jpg')  # читаем картинку
        cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty("window", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)  # ставим фулскрин
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img, 'Your last score ' + pred_res + ' And You gain ' + str(int(accuracy)) + '%', (10, 300), font, 0.6, (1, 255, 255), 2, cv2.LINE_AA)
        cv2.imshow('window', img)  # пишем на картинке текст
        cv2.waitKey(-1)  # когда нажимается любая клавиша программа завершается
